---
layout: markdown_page
title: "Category Direction - GitLab Documentation Site"
canonical_path: "/direction/ecosystem/foundations/gitlab_docs/"
---

- TOC
{:toc}

## GitLab Documentation Site

|                       |                               |
| -                     | -                             |
| Stage                 | [Ecosystem](/direction/ecosystem/)      |
| Maturity              | N/A |
| Content Last Reviewed | `2022-01-14`                  |

## Overview

Our goal is to create documentation that is complete, accurate, and easy to use. It should be easy to browse or search for the information you need, and easy to contribute to the documentation itself. All standards and practices for contributing documentation are found in the [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/) section of the docs site.

GitLab documentation helps users, admins, and decision-makers learn about using GitLab features, and how to implement and use GitLab to meet their [DevOps needs](/topics/devops/). Its source is developed and stored with the product in its respective paths within the GitLab [CE](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc), [EE](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc), [Runner](https://gitlab.com/gitlab-org/gitlab-runner/tree/master/docs), and [Omnibus](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc) repositories. The documentation is published at [docs.gitlab.com](https://docs.gitlab.com/) (offering multiple versions of all products’ documentation) and at the `/help/` path on each GitLab instance’s domain, with content for that instance’s version and edition.

### Target audience

**GitLab Users:** GitLab administrators and users rely on accurate, up-to-date, and comprehensive documentation of available features.

**GitLab Team Members:** GitLab team members are both contributors to and consumers of the documentation. While the [Technical Writing](https://about.gitlab.com/handbook/product/technical-writing/) team owns the documentation, all team members can and do contribute to the docs.

**Leadership & Decision-makers:** The GitLab documentation site is a valuable resource for decision-makers to compare features across versions and evaluate implementation details that may impact a purchasing decision.

### Challenges to address

A recent round of UX research highlighted a few key opportunities for improving the GitLab documentation site:

- In general, it’s hard for people to find the information they’re looking for. 
- Once they find it, over 90% of them say it's useful.  
- Users want more contextual **_why_** information.
- Troubleshooting info is the hardest to find.
- It’s confusing that /help and docs.gitlab.com are different sites and different experiences. 
- Almost 25% of docs site visitors are there for the first time.
- The number of new GitLab users who visit the docs site is rising.

Other problems not noted by research, such as a lack of context-sensitive Help and higher level tasks beyond single feature usage, are also addressed in the [Documentation roadmap](https://gitlab.com/groups/gitlab-org/-/epics/4602).


### Where we are headed

Our current focus is on improving the findability of information, refactoring the topic structure of pages on the docs site, and polishing the information architecture. We are also investigating improvements to the search function on the docs site.


### What's next and why

As outlined in the [Documentation Roadmap epic](https://gitlab.com/groups/gitlab-org/-/epics/4602) and in quarterly OKRs, a number of improvements are prioritized to address the challenges listed above. 

In addition, we're planning to invest in an [improved search experience](https://gitlab.com/groups/gitlab-org/-/epics/4656), making tutorial content easier to find, and designing [context-aware documentation](https://gitlab.com/gitlab-org/technical-writing/-/issues/335). 

### What's not planned right now

At this time, we are not investigating any significant architectural changes to the documentation site itself, like migrating to a new static site generator.

The Technical Writing team is not currently investing in localization of the documentation.

### Maturity plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->


Currently, the GitLab Documentation Site category is a *non-marketing category*, which means its maturity does not get tracked.


<!--
### User success metrics
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->


<!--
### Why is this important?
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

* [Stripe's documentation](https://stripe.com/docs) is considered the gold standard of documentation sites
* [Algolia](https://www.algolia.com/doc/) has excellent documentation and information architecture


<!--

### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
